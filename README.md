# speedy

This is the home of a static blog I've written using Rust to generate
all of the requisite HTML.

It's meant to be an experiment in using largely pure HTML and CSS, with
only minimal JavaScript, to create an extremely fast, accessible, and
easy to read website.

The site is deployed at https://blog.mplanchard.com

## Dependencies

If you have nix and direnv, all deps will be automatically installed and
available when you run `direnv allow`.

Otherwise, you'll need rust, gnu make, fd, and watchexec.

## Running

Build with `make`

Run with `make run`

Watch with `make watch`

## License

All of the code here is licensed under the Mozilla Public License 2.0,
and the blog posts (which you can find in the `posts/` directory) are
licensed under the Creative Commons Attribution-ShareAlike 4.0
International License
