POSTS := $(shell fd . --type f --extension md posts)
SRC := $(shell fd . --type f --extension rs --extension toml --extension lock)
SITE := $(shell fd . --type f templates static)

PUBLIC := $(shell fd . --type f public)

# If there is an error while executing a command to build a target,
# delete the built target to ensure that nothing gets corrupted and that
# the target will be rebuilt the next time make is run.
.DELETE_ON_ERROR:

# Do all commands in a target in a single shell.
.ONESHELL:

# Use bash with the -euo and pipefail options
.SHELL: $(shell which bash) -euo pipefail

# Default target. Build the site fresh and clean.
all: build
.PHONY: all

# Ensure we've built our Rust crate & generated our static site.
# Ensures a clean build by ripping everything out first.
build: clean $(PUBLIC)
.PHONY: build

# Everything in the public directory is generated via cargo run generate
$(PUBLIC) &: $(SRC) $(POSTS) $(SITE)
	cargo run generate

# Remove generated files
clean:
	rm -rf public
.PHONY: clean

# Rule to run the server.
# Will regenerate HTML if needed.
run: $(PUBLIC)
	cargo run run
.PHONY: run

# Watch for changes and run the server.
# Upon changes, regenerate posts if necessary and then rerun the server.
watch:
	cargo run run &
	trap 'jobs -p | xargs kill' EXIT
	watchexec \
		--watch posts \
		--watch templates \
		--watch static \
		--watch src \
		--watch Cargo.toml \
		cargo run generate
.PHONY: watch

post:
	read -p "Post Title: " TITLE
	if [[ "$${TITLE// }" == "" ]]; then
		echo -e "No title provided."
		exit 1
	fi
	SLUG=$$(echo "$$TITLE" |
		sed --regexp-extended 's/[  ]+/-/g' |
		sed 's/[(),.!:]//g' |
		awk '{ print tolower($$0) }')
	DATE=$$(date --iso-8601)
	FNAME=$$(echo "$${DATE}-$${SLUG}.md")
	FPATH=$$(echo "posts/$${FNAME}")
	if [[ -e "$$FPATH" ]]; then
		echo "$$FPATH" already exists!
		exit 1
	fi
	echo "---" > "$$FPATH"
	cat <<EOF >> "$$FPATH"
	draft: true
	title: $$TITLE
	slug: $$SLUG
	created: $$DATE
	updated: $$DATE
	tags:
	summary:
	EOF
	echo "---" >> "$$FPATH"
	echo "Created new post in $$FPATH!"


# Directory for empty targets
.make:
	@echo "Creating .make directory for target invocation tracking"
	mkdir -p .make
