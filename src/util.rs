//! Utility functions

use std::{
    fs,
    path::{Path, PathBuf},
};

pub fn copy_directory_contents<T, U>(src: T, target: U) -> std::io::Result<()>
where
    T: AsRef<Path>,
    U: AsRef<Path>,
{
    for dir_entry in fs::read_dir(src)? {
        let dir_entry = dir_entry?;
        let file_type = dir_entry.file_type()?;

        let source_path = dir_entry.path();

        let mut target_path = PathBuf::from(target.as_ref());
        target_path.push(
            dir_entry
                .path()
                .file_name()
                .expect("could not get file name"),
        );

        if file_type.is_dir() {
            fs::DirBuilder::new().recursive(true).create(&target_path)?;
            copy_directory_contents(source_path, target_path)?;
        } else if file_type.is_file() {
            fs::copy(source_path, target_path)?;
        } else {
            panic!("Symlinks are not supported")
        }
    }
    Ok(())
}
