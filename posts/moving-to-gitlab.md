---
title: Moving to GitLab
slug: moving-to-gitlab
created: 2021-11-25
updated: 2021-11-25
tags: blog, gitlab
summary: I decided to move my blog to GitLab, both for the source and for hosting (via GitLab pages). This post goes into the rationale and process behind that decision.
---

# Moving to GitLab

As is typical, I sat down to write a new blog post and decided instead to engage
in some healthy yak shaving first and move the blog to GitLab.

Previously, the source code for the blog was hosted on [GitHub][blog-gh], and
the generated blog was hosted on Azure. The process of deploying to Azure was
kind of a pain, and the SSL configuration for a bucket in Azure was needlessly
difficult. 

Last week at SpecTrust, we had an [innovation week], during which I set up the
SpecTrust engineering blog, which you can read more about [here][engineering-blog-intro].
We are in the process of moving our CI to GitLab at SpecTrust, and are
considering moving our code there too, so it seemed like a good idea to put the
blog project there, giving our engineers a chance to get the feel of using GitLab
for a small project to help inform our broader decision.

For our engineering blog, we used a pre-made static site generator called
[hugo], which worked out nicely, but what really impressed me was how easy it
was to get a GitLab Pages build up and running and behind our own custom domain.
The CI pipeline is dead simple. Look at the [new pipeline for this
site](https://gitlab.com/mplanchard/mplanchard.gitlab.io/-/blob/master/.gitlab-ci.yml),
for example. They handle getting a LetsEncrypt certificate for you, so all
you've got to do is point your DNS provider to your GitLab pages URL and you're
done. 

While I love the bespoke Rust static site generate I use for this site, the
process of manually deploying to Azure has always been a bit of a hassle, and I
never bothered to set up the secrets necessary to get deployment into my CI
pipeline. When I sat down today to write a new post, I couldn't help but be a
little envious of the situation with our blog at SpecTrust, so I decided to
spend some time porting to GitLab.

The process was straightforward. I created a new project in GitLab with the
appropriate name so that it would be served from `mplanchard.gitlab.io` rather
than `gitlab.io/mplanchard/project`, added it as the new default remote, and
pushed up my existing code. From there, I had been checking in the generated
content, so I removed that, updated the generator to go to a `public/`
directory, added the `.gitlab-ci.yml` file, and voila. I set up custom domains
for all the top-level domains that redirect to my blog: [www.mplanchard.com],
[blog.mplanchard.com], and [mplanchard.com]. I already had Cloudflare set up so
that www and bare `mplanchard.com` redirected to `blog.mplanchard.com`, so I
just had to update the latter to point to `mplanchard.gitlab.io`, and I was
done.

Now, whenever I'd like to post, all I have to do is push to the repo.

I also took the opportunity to add development environment management via `nix`,
which I'll talk about in more detail in another post.

[blog.mplanchard.com]: https://blog.mplanchard.com
[blog-gh]: https://github.com/mplanchard/speedy
[engineering-blog-intro]: https://engineering.spec-trust.com/posts/introduction/
[hugo]: https://gohugo.io/documentation/
[innovation week]: https://engineering.spec-trust.com/posts/innovation-week-2021/
[mplanchard.com]: https://mplanchard.com
[www.mplanchard.com]: https://www.mplanchard.com

