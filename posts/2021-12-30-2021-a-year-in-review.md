---
title: 2021: A Year in Review
slug: 2021-a-year-in-review
created: 2021-12-30
updated: 2021-12-30
tags: general, year-in-review
summary: 2021 was quite a year. From a global perspective it was difficult, to 
say the least. For me personally, it was a year of significant change, with 
much of that change being positive, thankfully. In this post, I'll take a look 
back at the major developments in my life over the course of 2021.
---

# 2021: A Year in Review

2021 was quite a year. From a global perspective it was difficult, to say the
least. For me personally, it was a year of significant change, with much of that
change being positive, thankfully. In this post, I'll take a look back and cover
the major developments in my life over the course of 2021. 

## Jobs

### Leaving Bestow

I left [Bestow](https://www.bestow.com/) in April of this year, after just over
two years. When I joined, I was the sixth or the seventh engineer in total, I
think, and the the third brought in to work on an in-progress rewrite of the
application backend, with the architect of the rewrite having already gone on to
greener pastures. When I left, we had somewhere in the vicinity of 30-40
engineers plus a substantial bevy of contractors, along with a corresponding
growth in the total number of employees from something like 20 to something
like 120. The number of policies sold per month increased by three orders of
magnitude in the time I was there, all supported by the same Python application
backend, newly integrated with a suite of freshly minted go microservices.

I really enjoyed my time there: there were lots of interesting technical
problems to solve, and the engineering culture was initially one of the best
I've experienced. However, the extreme rate of growth led to a pretty extreme
breakdown of the culture I had previously enjoyed, while also leading to
substantial difficulties in organizing cross-team work and collaboration.
Ultimately what drove me to leave Bestow was what I felt to be an ethical
mismatch between myself and the leadership of the company after the completely
incompetent or otherwise actively malicious treatment of a friend and colleague.
There aren't a lot of situations I can imagine that would be immediate
deal-breakers for me, but this was certainly one of them.

I learned a lot at Bestow, and indeed I wrote another post today, [What I Learned at
Bestow](/posts/what-i-learned-at-bestow.html), after this section of this post
got much too long. Please check that out for more details.

### Joining SpecTrust

I took a significant pay cut and title reduction (\$165k -> \$140k and Staff
Engineer -> Senior Engineer) in order to join
[SpecTrust](https://www.spec-trust.com/) as (non-executive) engineer number 2
and employee number 6, and I could not be happier with my decision. I really
wanted to work with Rust, and with SpecTrust's core product being written in
Rust, I have had plenty of opportunity to do so. I am closely aligned with the
executive team philosophically and technically, and I have faith in the
technical and product vision at the company. Truth be told, I was leaning
towards another opportunity until I got a product demo from our now VP of
Engineering. Seeing the product in action helped me realize how revolutionary it
is, and I was (and am) super excited to be able to work on it. This is the only
startup I've worked at so far where I really believe in the product.

Since joining SpecTrust, I've managed to help bring in some of my favorite
colleagues from prior jobs, and between them and our new hires, we've built a
team that I'm totally chuffed to be able to work with every day. I'm energized
by the technical challenges, I enjoy the tech we're working with, and I am inspired
by my coworkers. I've always said that work satisfaction is essentially divided
into thirds: who you work for, who you work with, and what you do. Here, at least
for now, I'm lucky enough to be satisfied with all three.

## Tech

### Rust

This has really been the year of Rust for me, personally, going from using it
for side projects to working in it professionally. I plan on doing a more
in-depth retrospective once I've been using it professionally for a year, but as
of now my feelings are even more positive than I would have expected them to be
eight months ago. There are not many languages that I can imagine being more
pleasant to work in, which comes from a range of factors:

- Expressive type system allows pulling substantial logic into compile-time
  verification.
- "If it compiles, it works" gives a constant, warm feeling of safety.
- Top-notch language design: traits, sum types, pattern matching,
  immutability-by-default, error handling, and inline unit tests stand out in particular.
- Incredible performance means we can use significantly fewer cloud resources
  while serving substantially more traffic.
- The cargo-driven build system (and dependency management) is a pleasure to
  work with, being head and shoulders above not only classical build systems for
  e.g. C++ but also modern build systems for dynamic languages like `pip` and
  `npm`.
- "Fighting the borrow checker" steadily evolves into "understanding much more
  about the code," "designing it right from the start," and "being super
  grateful to the borrow checker for everything it catches."
- It's super easy to split code out into libraries where it makes sense.
- Broad applicability across tons of domains (embedded, web, graphics, HPC,
  etc.) means that everything I learn can apply to whatever type of programming
  I would like to do.

Of course, nothing is perfect, and Rust has its share of rough edges. Things I
can think of off the top of my head:

- Async is still a bit awkward, especially in its interaction with closures. The
  [FutureExt](https://docs.rs/futures/0.3.19/futures/future/trait.FutureExt.html)
  and [TryFutureExt](https://docs.rs/futures/0.3.19/futures/future/trait.TryFutureExt.html) 
  traits help a lot with this, but you've got to know they exist, and they take
  a fair bit of noodling to understand and come up to speed on. Runtime
  compatibility issues are _almost_ a thing of the past now that Tokio is on
  1.0, and I'm confident the async story will continue to improve.
- The builtin test framework could be a little more fully featured (exactly once
  test setup (e.g. migrating a database) across multiple integration tests is
  quite hard to do correctly).
- Runtime debugging of async issues is hard, but projects like [Tokio
  Console](https://tokio.rs/blog/2021-12-announcing-tokio-console) are stepping
  up to fill the void
- Refactoring lifetimes can be a real pain in the butt. For example, introducing
  a lifetime to a struct definition can require tons of updates everywhere the
  struct is used. This wouldn't be so bad, except that error checking is done in
  stages, and once the first stage is done (checking that each struct has the
  right number of lifetime references), you may wind up discovering that your
  new lifetime causes some insurmountable ownership issue back in a method of
  the original struct and having to go back to the drawing board. I'm not sure
  what the solution is here. Fewer passes for error checking would certainly help.

I'm more than willing to live with the rough edges and nits, because I've never
enjoyed a language this much. Also, everyone complains about compile times, but
once we added `sccache` and made a few tweaks like enabling split debug info, I
haven't generally had any cause for frustration during day-to-day development.

### Linux

As part of the move to SpecTrust, I switched from using a Mac for work to once
again using Linux, after having switched my personal computers over in 2020 (Big
Sur was just too much for me). It feels great to be back on Linux. There are
definitely hassles here and there, but the control, customizability, and freedom
are more than worth it. I'm writing this post via Sway, a tiling window manager
for Wayland, but I can also switch over to i3 with X if needed, or into Gnome if
I need the whole kit and kaboodle. The only thing I haven't quite been able to
get working in Sway is screen sharing via Zoom, but I've got leads there.

Given the things I usually do on the computer (programming, basic photo
management, web browsing), there's really no reason not to use Linux as my
primary driver, although I'll probably still keep a Mac around for a while to
debug any compatibility issues we run into with the development environment at
work.

### Emacs

I had been using Emacs for a while for note taking, but I started using it for
all of my programming some time in mid to late 2020, so 2021 has been a full
year completely in the Emacs world. I'm familiar enough with it now that I can
usually track down and fix any issues with third-party packages without too much
trouble, and there aren't very many things that I don't know how to do from a
customization perspective. Between native compilation in emacs 28 and continued
updates from the LSP folks, the performance issues I used to have when working
on large codebases have disappeared. I've played with using Emacs as an email client, 
a music player, a HackerNews reader, a database explorer, a blogging platform, a
GitHub controller, and much more. Meanwhile, org-mode with org-roam continues to
be the pinnacle of note-taking software.

Emacs has really shifted the way I think about interacting with my computer. I
now generally have only two applications open: Emacs and Firefox (okay and
sometimes Zoom). I view Emacs as the primary interface to everything on my local
(or remote) machines, while Firefox is the interface to Internet. Fundamentally,
those two contexts are all I work with, so Emacs + Firefox is plenty. The
consistency of accessing all of my local stuff in Emacs means that I get the
same shortcuts, muscle memory, and ability to script workflows for terminal
operations, directory traversal, file operations, and of course writing text/code.

I think at this point I'm thoroughly stuck. It's hard to imagine giving up the
level of cohesion and easy customization of my environment that I get from
Emacs, so I imagine I'll continue to use it as the primary interface to my
computer through 2022.

### Janet

In general, 2020 and 2021 saw me fairly disillusioned with Python as a language
for large projects, and my experience with the constant upgrade treadmill made
me feel like it's not even a great language for scripting anymore (Do I use the
`python` or `python3` executable? Which version of python 3 am I able to
support? Etc.). As a result, I've been on the lookout for nice solutions for
scripting languages. I've gotten much more familiar with Lisp over the course of
2021, largely due to Emacs, and I really have come to enjoy the Lisp family of
languages. So, my interest was piqued when I came across [JanetLang](https://janet-lang.org/),
which is an easily embeddable language with a big enough standard library to
actually be useful, easy C interop (and thus Rust interop), and great support
for parallelism out of the box. It's still early stages for Janet as of yet:
even in Emacs, the canonical home of Lisps, editing could be better. [`janet-mode`](https://github.com/ALSchwalm/janet-mode)
provides the basic S-expression-based editing and syntax highlighting, and [`ijanet-mode`](https://github.com/SerialDev/ijanet-mode)
provides an interactive REPL, but function lookup and autocomplete are sorely
missed. 

For now, something like [`rust-guile`](https://docs.rs/crate/rust-guile/0.1.5)
may be the best bet for embeddable scripting, but I'm keeping a close eye on
Janet!

<div style="text-align: center">
    <img src="https://media.giphy.com/media/xUOxeRRkTYdQJfyy2Y/giphy.gif" alt="Janet" width="25%">
</div>

### Nix

In 2021, I moved most of my local environment setup to use
[home-manager](https://github.com/nix-community/home-manager/). I also used nix
to set up the local development environment for SpecTrust's primary repo, and
set up nix development environments for some of my side projects. You can see my
post on [adding nix to the blog](/posts/nixifying-the-blog.html) for a full
exploration of that process.

Nix is amazing. There really is no better solution for specifying cross-platform
system packages. Guix might could be better, since guile is inarguably a more
pleasant language than the nix expression language, but guix is not usable on
MacOS, and so limits its cross-platform applicability.

Nix's amazingness is somewhat tempered by its absolutely ridiculous learning
curve and godawful documentation. Looking up a commonly used function on the web
is basically impossible: I have found that the best way to figure things out is
to clone `nixpkgs` locally and go a-grepping. The community is, to my
understanding, aware of this and working on solutions, but just at minimum
requiring doc comments on all the functions and making those accessible in a web
interface would go a long way.

I am also using nix for a relatively limited subset of its total functionality.
It is difficult enough to learn that I am hesitant to introduce it too much more
deeply into our engineering process at SpecTrust, despite its clear benefits.
Hopefully they'll improve the onboarding experience a bit and that hesitation
will go away in 2022.

## Personal

2021 was the year of the vaccine! We got vaccinated as early as we could, which
wound up being early May for the second dose. For us, this unlocked both a fair
bit of comfort with doing things like having other vaccinated friends over,
eating in restaurants, and going on trips.

Unfortunately, we somehow managed to arrange two of our three trips out of town
this year with the rise of new variants. In late July, we went up to Michigan in
Traverse City for a week to spend some time with a friend and colleague who was
just joining SpecTrust, and then we went to Cape Cod with some of my wife's
family for a week. Of course, about midway through the trip, the delta variant
really started ot take off. Nonetheless, we stayed relatively careful and had a
great time.

We also went to see my mom and my aunt in the Houston area in November. We then
got our boosters, and, just in time for the rise of the Omicron variant, we left
town on December 4th to spend a week in San Jose for work, which was my first
time meeting many of my colleagues in person and was a great time. We then drove
up to the Redwoods National Park area and spent a few days hiking before coming
back down to San Francisco to do Christmas shopping and to get in some great
meals before we came home. We then popped over to New Mexico to spend Christmas
with my wife's family, and we finally got home yesterday (the 28th). I'll
consider myself lucky if we managed to do all that traveling without catching
it. Luckily, we traveled via Amtrak, so we had private rooms for most of the
journey, and lots more space between us and other passengers in coach than we
would have had on an airplane. We're now self-isolating for a couple of weeks to
make sure we don't come down with anything before we see anybody here.

News about Omicron is looking more encouraging than it could be. Hopefully that
trend continues, and we can get back to something like normal over the next weeks.

## Looking Ahead

There's a lot to look forward to in the next year!

### Tech/Projects

#### NixOS

I've been meaning to try out NixOS for ages, and I feel like I may finally have
enough of a grip on the ecosystem and expression language to make it happen.
Very possible I'll wind up just going right back to something else (probably
Arch), but it's worth a shot.

#### Orgdown Parser

[Orgdown](https://gitlab.com/publicvoit/orgdown) is an attempt to separate the
org-mode language specification from its implementation, specifying different
tiers of support, where the lowest tier is basic syntax and the highest tier is
essentially the full org-mode feature set. I'd like to try my hand at writing a
parser in rust, to make it easier to support org-mode editing in editors outside
of Emacs. I think org-mode is one of the best points of entry for Emacs, but
even getting started with it can be really intimidating. Basic support in VSCode
and other editors would I think ease the transition, and it would be cool to
eventually make something like a standalone org-mode editor.

#### Elm

[Elm](https://elm-lang.org/) is an ML-inspired frontend language. I've played
with it a bit, but I'd like to make a real project in it. Its general style and
focus aligns well with Rust, and it would be interesting to see if we could get
some Elm into the FE at SpecTrust.

### Personal

The main thing that we're trying to do in the upcoming year is figure out where
we want to move to. We had been talking about it for a while, but after last
year's freeze and the whole political _situation_ at the moment, we've decided
we would like to get out of Texas. As much as we like Austin, it can't make up
for being in Texas.

We're primarily looking in the Vermont area, so we'll at some point travel up to
the Northeast to get a feel for different areas and look at real estate.
However, depending on the COVID and Visa situations, we may try to first spend a
some time in France, since we are unlikely to have another opportunity to do so
in the near term.

I'm also hopeful that this year we can see the tail end of COVID. With the
Omicron variant being substantially more transmissible than previous variants,
it's possible we'll get to a point where the vast majority of people worldwide
have been either infected or vaccinated, and things can get back to normal.
We'll see, I suppose.

