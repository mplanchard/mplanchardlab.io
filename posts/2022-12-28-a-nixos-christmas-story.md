---
draft: false
title: A NixOS Christmas Story
slug: a-nixos-christmas-story
created: 2022-12-28
updated: 2022-12-28
tags: nix, nixos
summary: A quick story of how NixOS saved Christmas
---

# A NixOS Christmas Story

It was Christmas day, 2022, and it was time to do the crossword. Every Sunday,
we do the New York Times crossword with my mom, and this Sunday was no
exception. We had about an hour until we needed to leave for family Christmas
festivities, so we figured we'd pop on a video chat, solve the puzzle, and then
get to getting.

However, as we started the video chat, disaster struck. I could see my mom's
mouth moving, but no sound was coming out. Some quick debugging revealed the
issue was on my end and wasn't a problem with Zoom. Alas, there was no sound
coming out of the speakers at all, for any application!

"It must be something about the update I did last night," I mused. And yes, dear
reader, on Christmas Eve I had upgraded my flake to the NixOS 22.11 stable
branch, run a `nix flake update`, and rebooted into the new configuration.
This must have been the cause of my audio woes! I was certain of it. But it was
Christmas day! I didn't have time to go digging to figure out whether there was
an issue with my audio driver, whether my sound card was being recognized by the
kernel, or any other humbuggery! We only had an hour until we had to leave! Ah,
but all would be well, for NixOS would come to my rescue!

"Hang on, let me reboot," I texted to Mom, and then reboot the computer. When I
hit the boot loader, there was my prior configuration waiting for me, shining
with hope like Rudolph's nose right beneath the newest one, which helpfully
showed that it had been created on 2022-12-24. I selected the old config and
booted into my pre-upgrade system. A quick test showed the sound to be back
online! With less than five minutes' delay, we exchanged our Christmas greetings
and started the crossword. We were able to finish with plenty of time to spare.

And so, lo and behold, Christmas was saved, thanks to functional, declarative
system management and non-destructive upgrades! I shudder to think of what would
have happened had the same thing happened on any other operating system.

## Epilogue

Ultimately I'm not sure what the issue was. It didn't persist when I booted back
into the new config on Boxing Day, so I chalk it up to something transient
related to that first boot. It's possible it would have resolved itself on a
reboot even without switching back to the old config. Nonetheless, in the
moment, I was grateful for the ability to roll my system back so quickly and
with zero hassle!

