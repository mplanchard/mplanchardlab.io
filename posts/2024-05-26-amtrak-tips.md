---
draft: false
title: Amtrak Tips
slug: amtrak-tips
created: 2024-05-26
updated: 2024-08-18
tags: amtrak, travel
summary: A collection of tips and tricks for riding Amtrak
---

# Amtrak Tips

I have ridden the train a lot, because I love it. It's better for the
environment than flying, it gives you a real, visceral sense for the
distance you're covering, and it's a thousand times more comfortable
than an airplane.

I have found myself dispersing tips and tricks for riding the train
often enough that I felt like it would be worth consolidating them
into one place I could just send links to, so here it is. I will
update this post over time as things change or I think of new things
to write.

## Tickets

- The rewards program is worth it, so be sure to sign up and put your
  number in with all your purchases. The best rewards are companion
  passes (free second tickets) and free upgrades
- Business class is usually not worth it in my opinion
- First class on the Acela is really nice, definitely buy business
  class and use your free upgrades to get first class (or use two
  upgrades) when you get them as part of the rewards program

## On the Train

- There are drinking water spigots in most trains, generally on the
  wall by the bathrooms. Bring a bottle and refill to avoid having to
  overpay for water in the cafe car.

### The Cafe Car

- If you plan to purchase food on the train, it’s worth bringing cash
  as a backup for cards. I’ve only been on two trips where the card
  readers weren’t working, but it sucks to not be able to buy food
  when you’re hungry

## Stations, Boarding

- If you are in a large group or have a lot of bags, you can ask for
  Red Cap assistance to board early and ensure you all sit
  together. Please tip the red caps! I’d say $10 minimum, more if
  they’re helping you lug a bunch of bags.
- As part of the rewards program or any time you are traveling in a
  sleeper car, you get Amtrak lounge access. You can store your bags
  in the lounge in relative safety, which is a lifesaver for long
  layovers where you want to leave the station to go grab some food or
  whatever. You will also generally get to board early, at the same
  time as Red Cap boarding.
- With the possible exception of Moynihan Hall, which has surprisingly
  good food options, you can usually find better food than in the
  station within a 15 minute walk in most major or minor cities. Use
  long layovers to your advantage!

### Moynihan Hall (NYC)

- The NY Pizza Suprema right outside the station at 31st and 8th is
  very good (better than the pizza in the station) and inexpensive
- Blue Bottle Coffee in the main atrium has very serviceable espresso
  drinks
- The bar in the food court has a good selection of local draughts
- You can get to the subways without going aboveground by following
  signs for NJ Transit when you deboard
- Best way to get to JFK from Penn and vice versa is via LIRR: most of
  the LIRR routes pass through Jamaica Station, where you can pick up
  the Red Line to JFK. Takes about an hour, depending on your timing,
  and is faster than taking the MTA lines to Jamaica

### Chicago Union Station

- The Ogilvie Transportation Center is a short walk from Union
  Station, and it has a bunch of places to grab a quick bite,
  including Jian, which does a great Chinese street food called Jian
  Bing (highly recommended)
- Several good coffee shops nearby: Meddle Coffee Bar, Cupitol, and
  Espresso and Milk
- If you have a longer layover, the walk to the waterfront is easy:
  you can get great views of Lake Michigan, and you’ll pass all manner
  of places to have a bit, a coffee, or a drink

### Albany, NY

- No Amtrack lounge
- Seems like there are a good number of decent places to eat within a
  15 minute walk, but I’ve only been there on Sunday, when literally
  EVERY SINGLE ONE is closed, even the ones that say on Google Maps
  that they should be open
- The cafe in the station is overpriced and mediocre

### Schenectady, NY

For whatever reason this tiny station winds up being a common transfer
spot when traveling around New England. It is not my favorite place to
get stuck, mostly because the lack of an Amtrak lounge means I’ve got
to either cart my bags everywhere or, if traveling in a group, leave
someone with the bags while I go pick up food, rather than being able
to go all together.

- No Amtrak lounge here, which means you either have to lug your bags
  around or pay to have them stored at the front desk
- The adjacent food options are okay, but there are better things to
  find with the short walk to downtown

## Lines

## Ethan Allen Express

- From NYC (heading to upstate NY and Vermont), usually leaves from
  track 5/6. Go mill around there and get in the line that will be
  forming about 20 minutes before departure to be sure you get a good
  seat, because it fills up quick! Don’t wait for the track
  announcement, but do listen for it, because occasionally it will be
  on track 7/8 instead
- The train will switch directions in Rutland, VT. Sit on the left for
  views of the Hudson River coming out of the city and for several
  hours thereafter. Sit on the right for views of the Adirondacks
  later on (I recommend Hudson side because you get into VT pretty
  late). Swapping seats after Rutland if you don’t want to sit
  backwards is no problem, just take your destination ticket with you
  and put it above your new seat

## Overnight Trips or Longer

- Even if you don’t have Amtrak rewards status, traveling on a sleeper
  car gets you access to the Amtrak lounge at large stations, which is
  100% worth taking advantage of for luggage storage and early
  boarding
- East of the Mississippi, there is no longer a proper dining car with
  fresh-prepared food, just frozen meals that they throw in the
  microwave. It's worse in every way, but at least it is still free
  with your purchase of a sleeper car ticket
- Roomettes are a tight squeeze with two people, but are much cheaper
  than the bedroom. For a solo traveler they feel less tight
- If you are a large person in any dimension, the roomettes may be too
  cramped for you. I am 5’6, and while I at times would like more
  space, I am able to stretch out fully on the bed with no
  problems
- On some lines, the bathroom is _inside_ the roomette, which can be
  fairly awkward even with a spouse. Check the train type for your
  line
- Bedrooms have an integrated (private) bathroom, and are the most
  comfortable option if you can afford them
