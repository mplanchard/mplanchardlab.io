---
title: Rust to Webassembly to JavaScript
slug: rust-to-webassembly-to-javascript
created: 2021-11-25
updated: 2021-11-25
tags: rust, webassembly, javascript, typescript
summary: Cross-posting a post I did for our blog at work walking through the process of compiling a Rust project to webassembly such that it can be made available in a variety of JavaScript contexts, including both node and the browser, with an associated example repository.
---

# Rust to WebAssembly to JavaScript

I wrote a post for SpecTrust's engineering blog on compiling Rust projects to
WASM and then making that WASM available in JavaScrpit. In particular, I wrote
it to address what I felt like were some substantial holes in the existing
documentation, specifically around:

- Building WASM output that can be used by both node and browser projects
- Explicitly spelling out the difference between using WASM with different
  versions of webpack
- Using WASM in the very common React application context

If you're interested, check out the post
[here](https://engineering.spec-trust.com/posts/rust-to-webassembly-to-javascript/),
and the example repository [here](https://gitlab.com/spectrust-inc/awesome-wasm).

The blog's got an RSS feed, so subscribe if you'd like to see what I and the
rest of the engineering team put out in the future!

