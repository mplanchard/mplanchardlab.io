---
draft: false
title: A Zero Downtime Migration to Time-Partitioned Tables in Postgres
slug: zero-downtime-migration-to-partitioned-tables-in-postgres
created: 2024-08-18
updated: 2024-08-18
tags: postgres, data, partitions
summary:
    A crosspost from our engineering blog at work, in which I describe
    how we managed to migrate databases receiving millions of writes per
    day to time-partitioned tables without incurring any downtime or
    performance degradations.
---

# A Zero-Downtime Migration to Time-Partitioned Tables in Postgres

Check out [this post](https://engineering.specprotected.com/posts/changing-the-engine-while-the-car-is-running/)
from our company engineering blog, in which I describe the process of
migrating our databases to time-partitioned tables with no downtime
or performance degradation.

Speaking personally, this was one of the more interesting feats I've
pulled off in my career thus far. While the approach was relatively
straightforward, the actual application of it to production
environments was often a fair bit more difficult and stressful, and so
its going off without a hitch was really gratifying.
